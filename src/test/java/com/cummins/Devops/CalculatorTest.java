package com.cummins.Devops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
	
	
	@Test
	 public void addTest (){
        Calculator calculator=new Calculator();
        int result=calculator.add(2, 3);
        assertEquals(5, result);
    }

}
